import base_01 from "../assets/images/parts/Base/crab_01.png";
import base_02 from "../assets/images/parts/Base/goldfish_01.png";
import base_03 from "../assets/images/parts/Base/narwhal_01.png";
import base_04 from "../assets/images/parts/Base/starfish_01.png";

import eyes_01 from "../assets/images/parts/Eyes/big.png";
import eyes_02 from "../assets/images/parts/Eyes/happy.png";
import eyes_03 from "../assets/images/parts/Eyes/joy.png";
import eyes_04 from "../assets/images/parts/Eyes/wink.png";

import mouth_01 from "../assets/images/parts/Mouth/cute.png";
import mouth_02 from "../assets/images/parts/Mouth/happy.png";
import mouth_03 from "../assets/images/parts/Mouth/smirk.png";
import mouth_04 from "../assets/images/parts/Mouth/surprised.png";


export const parts = {
  base: [base_01, base_02, base_03, base_04],
  eyes: [eyes_01, eyes_02, eyes_03, eyes_04],
  mouth: [mouth_01, mouth_02, mouth_03, mouth_04],
};
