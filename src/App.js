import React, { useEffect, useState } from "react";
import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { connect } from "./redux/blockchain/blockchainActions";
import { fetchData } from "./redux/data/dataActions";
import * as Styles from "./styles/globalStyles";
import PlayerRenderer from "./components/playerRenderer";
import ProgressBar from 'react-bootstrap/ProgressBar'
import 'bootstrap/dist/css/bootstrap.min.css';
import rarityStar from './assets/images/rarity/yugioh_star.png'
import nftlogo from './assets/images/icon/NFT_Icon.png';
function App() {
  const [loading, setLoading] = useState(false);
  const data = useSelector((state) => state.data);
  const blockchain = useSelector((state) => state.blockchain);
  const dispatch = useDispatch();
  const [selectedTab, setSelectedTab] = useState("mynfts");

  const APPTABS = ["mynfts", "worldnfts"];
  useEffect(() => {
    if (blockchain.account !== "" && blockchain.playerToken !== null) 
    {
      dispatch(fetchData(blockchain.account));
    }
  }, [blockchain.playerToken]);

  function mintNFT(account, name){
    setLoading(true);
    blockchain.playerToken.methods.createRandomPlayer(name).send({
        from: account,
        value: blockchain.web3.utils.toWei("0.01", "ether"),
      }).once("error", (error) => {
        setLoading(false);
        console.log(error);
      }).then((receipt) => {
        setLoading(false);
        console.log(receipt);
        dispatch(fetchData(blockchain.account));
      });
  };

  function feedPlayer(account, playerid){
    setLoading(true);
    blockchain.playerToken.methods.feedPlayer(playerid).send({
        from: account,
      }).once("error", (error) => {
        setLoading(false);
        console.log(error);
      }).then((receipt) => {
        setLoading(false);
        console.log(receipt);
        dispatch(fetchData(blockchain.account));
      });
  };

  function calculateRarity(rarity)
  {
    var stars = [];
    for(var i = 0; i < parseInt(rarity / 10); i++)
    {
      stars.push(1);
    }
    if(stars.length == 0)
    {
      stars.push(1);
    }
    return stars;
  }

  function handleTabClick(e) 
  {
    /*e.target.classList.add("selectedTab");
    for(var i = 1; i < 3; i++)
    {
      var tabIDString = "app_tab";
      tabIDString += i;
      console.log(tabIDString);
      if(tabIDString == e.target.id)
      {
        continue;
      }
      var element = document.getElementById(tabIDString);
      if(element)
      {
        element.classList.remove("selectedTab");
      }
    }*/
    for(var i = 0; i < APPTABS.length; i++)
    {
      if(e.target.id == APPTABS[i])
      {
        setSelectedTab(APPTABS[i]);
        break;
      }
    }
  }
  return (
    <div className = "app_screen">
      {blockchain.account === "" || blockchain.playerToken === null ? (
        <Styles.Container flex = {1} ai = {"center"} jc = {"center"}>
          <div className = "app_connect">
          <img src = {nftlogo} alt = "logo" style = {{width: "100px", heigth: "100px"}}/>
          <Styles.TextTitle>Connect to the NFT platform </Styles.TextTitle>
          <Styles.SmallSpace />
          <p style = {{color: "white"}}>Beta V1.0</p>
          <Styles.SmallSpacex2 />
          <button className = "button"
            onClick = {(e) => {
              e.preventDefault();
              dispatch(connect());
            }}
          >
            CONNECT
          </button>
          <Styles.SmallSpace />
          {blockchain.errorMsg !== "" ? (
            <Styles.TextDescription>{blockchain.errorMsg}</Styles.TextDescription>
          ) : null}
          </div>
        </Styles.Container>
      ) : (
        <div className = "app_container">
        <div className = "app_header">
          <a id = "link_home" href = "#">Home</a>
          <a id = "link_play" href = "#">Marketplace (Coming Soon)</a>
          <a id = "link_tbd" href = "#">Coming Soon</a>
        </div>
        <div className = "app_userinfo">
          <div>
              My {data.allOwnedPlayers.length} {data.allOwnedPlayers.length > 1 ? "players" : "player"}
          </div>
          <div>
            stuff
          </div>
        </div>
        <ul className = "app_tabs">
          <li onClick = {handleTabClick} id = "mynfts" className = {selectedTab === "mynfts" ? "selectedTab" : null}>My Player NFTs</li>
          <li onClick = {handleTabClick} id = "worldnfts" className = {selectedTab === "worldnfts" ? "selectedTab" : null}>World Player NFTs</li>
        </ul>
        <Styles.Container style = {{ padding: "24px" }}>
          <Styles.Container ai = {"center"}>
          <Styles.TextTitle>{selectedTab === "mynfts" ? "Welcome to my NFT platform" : "All existant Player NFTs are listed here" }</Styles.TextTitle>
          <Styles.SmallSpacex2 />
          {selectedTab === "mynfts" ? <><button className = "button purple mint" disabled = {loading ? true : false} onClick = {(e) => {
              e.preventDefault();
              mintNFT(blockchain.account, document.getElementById("playername").value);
              document.getElementById("playername").value = "";
            }}>
            Create NFT
          </button> 
          <input className = "app_nameinput" id = "playername" type = "text" placeholder = "type name here" disabled = {loading ? 1 : 0}/>
          </>: null }
          {loading ? <p className = "app_minting_text" style = {{color: "white"}}>Minting your new NFT...</p>: null}
          {!loading ? data.allOwnedPlayers.length === 0 ? <p style = {{color: "white", marginTop: "5%"}} className = "app_minting_text">We noticed you have no NFTs, try minting one with the Create NFT button.</p> : null : null}
          </Styles.Container>
          <Styles.MediumSpace />
          <Styles.Container jc = {"center"} fd = {"row"} style = {{ flexWrap: "wrap"}} >
            {selectedTab === "mynfts" ? <>{data.allOwnedPlayers.map((player, index)  => {
              return (
                <div key = {index} className  = {player.nRarity >= 90 ? "player__container super_rare" : "player__container"} style = {{ padding: "15px", margin: "15px", borderRadius: "25px", backgroundColor: "rgb(105, 2, 255)" }}>
                  <PlayerRenderer player = {player} />
                  <Styles.SmallSpace />
                  <Styles.Container ai = {"none"} jc = {"center"} style = {{ textAlign: "center", color: "#ffe701", fontFamily: "AmericanCap"}}>
                    <span>#{player.nID}</span>
                    <span>DNA: {player.nDNA}</span>
                    <span>LEVEL: {player.nLevel}</span>
                    <span>NAME: {player.name}</span>
                    <span>RARITY: 
                      {calculateRarity(player.nRarity).map((stars, id)  => 
                          <img key = {id} alt = {player.name} src = {rarityStar} style = {{width: "10px", heigth: "10px", margin: "3px"}}/>
                      )}
                    </span>
                    <Styles.SmallSpace />
                    <ProgressBar animated = {true} now = {player.nAmountFed} active = "true"/>
                    <Styles.SmallSpace />
                    <Styles.SmallSpace />
                    <button className = "button purple" disabled = {loading ? true : false} onClick = {(e) => {
                      e.preventDefault();
                      feedPlayer(blockchain.account, player.nID);
                    }}>
                    Feed
                  </button>
                  </Styles.Container>
                </div>
              );
            })}</> : <>{data.allPlayers.map((player, index)  => {
              return (
                <div key = {index} className  = {player.nRarity >= 90 ? "player__container super_rare" : "player__container"} style = {{ padding: "15px", margin: "15px", borderRadius: "25px", backgroundColor: "rgb(105, 2, 255)" }}>
                  <PlayerRenderer player = {player} />
                  <Styles.SmallSpace />
                  <Styles.Container ai = {"none"} jc = {"center"} style = {{ textAlign: "center", color: "#ffe701", fontFamily: "AmericanCap"}}>
                    <span>#{player.nID}</span>
                    <span>DNA: {player.nDNA}</span>
                    <span>LEVEL: {player.nLevel}</span>
                    <span>NAME: {player.name}</span>
                    <span>RARITY: 
                      {calculateRarity(player.nRarity).map((stars, id)  => 
                          <img key = {id} alt = {player.name} src = {rarityStar} style = {{width: "10px", heigth: "10px", margin: "3px"}}/>
                      )}
                    </span>
                    <Styles.SmallSpace />
                    <ProgressBar animated = {true} now = {player.nAmountFed} active = "true"/>
                    <Styles.SmallSpace />
                    <Styles.SmallSpace />
                    {selectedTab === "mynfts" ? <button className = "button purple" disabled = {loading ? true : false} onClick = {(e) => {
                      e.preventDefault();
                      feedPlayer(blockchain.account, player.nID);
                    }}>
                    Feed
                  </button> : null}
                  </Styles.Container>
                </div>
              );
            })}</>}
          </Styles.Container>
        </Styles.Container>
        <div className = "app_footer">
          <p>
            {blockchain.account}
          </p>
          <p>
            Total Player NFTs in the world: {data.allPlayers.length}
          </p>
        </div>
        </div>
      )}
    </div>
  );
}

export default App;