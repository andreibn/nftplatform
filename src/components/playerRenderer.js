import React from "react";
import { parts } from "../parts/parts";
import rarity1 from "../assets/images/rarity/_rarity_1.png";
import rarity2 from "../assets/images/rarity/_rarity_2.png";
import rarity3 from "../assets/images/rarity/_rarity_3.png";

const PlayerRenderer = (props) => {
  if(!props.player) 
  {
    return null;
  }
  const DEFAULTSIZE = 200;
  const DNA_DIGITS = 15;
  let rarity = rarity1;

  if (props.player.nRarity >= 60) 
  {
    rarity = rarity2;
  }
  if (props.player.nRarity >= 90) 
  {
    rarity = rarity3;
  }

  let dnaString = String(props.player.nDNA);
  if(dnaString.length < DNA_DIGITS)
  {
    return null;
  }

  let playerDetails = {
    base: dnaString.substring(0, 2) % 4,
    eyes: dnaString.substring(2, 4) % 4,
    mouth: dnaString.substring(4, 6) % 4,
    background: dnaString.substring(6, 8) % 5,
  };

  const playerStyle = {
    width: "100%",
    height: "100%",
    position: "absolute",
  };
  const backgrounds = ["blue", "red", "yellow", "green", "purple"];

  return (
    <div style={{minWidth: props.size ? props.size : DEFAULTSIZE, minHeight: props.size ? props.size : DEFAULTSIZE, background: backgrounds[playerDetails.background], position: "relative", borderRadius: "25px", ...props.style,}}>
      <img alt={"base"} src={parts.base[playerDetails.base]} style={playerStyle} />
      <img alt={"eyes"} src={parts.eyes[playerDetails.eyes]} style={playerStyle} />
      <img alt={"mouth"} src={parts.mouth[playerDetails.mouth]} style={playerStyle} />
      <img alt={"rarity"} src={rarity} style={playerStyle} />
    </div>
  );
};

export default PlayerRenderer;
