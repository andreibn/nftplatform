import store from "../store";

const fetchDataRequest = () => {
  return {
    type: "CHECK_DATA_REQUEST",
  };
};

const fetchDataSuccess = (payload) => {
  return {
    type: "CHECK_DATA_SUCCESS",
    payload: payload,
  };
};

const fetchDataFailed = (payload) => {
  return {
    type: "CHECK_DATA_FAILED",
    payload: payload,
  };
};

export const fetchData = (account) => {
  return async (dispatch) => {
    dispatch(fetchDataRequest());
    try 
    {
      let allPlayers = await store.getState().blockchain.playerToken.methods.getPlayers().call();
      let allOwnedPlayers = await store.getState().blockchain.playerToken.methods.getOwnedPlayers(account).call();
      dispatch(
        fetchDataSuccess({
          allPlayers,
          allOwnedPlayers,
        })
      );
    } 
    catch (error) 
    {
      console.log(error);
      dispatch(fetchDataFailed("Could not load data from the smart contract."));
    }
  };
};
