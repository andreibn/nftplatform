import { applyMiddleware, createStore, combineReducers } from "redux";
import thunk from "redux-thunk";
import blockchainReducer from "./blockchain/blockchainReducer";
import dataReducer from "./data/dataReducer";

const allReducers = combineReducers({
  blockchain: blockchainReducer,
  data: dataReducer,
});

const store = createStore(allReducers, applyMiddleware(thunk));

export default store;
