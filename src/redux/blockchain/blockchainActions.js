import Web3 from "web3";
import PlayerToken from "../../contracts/PlayerToken.json";
import { fetchData } from "../data/dataActions";

const connectRequest = () => {
  return {
    type: "CONNECTION_REQUEST",
  };
};

const connectSuccess = (payload) => {
  return {
    type: "CONNECTION_SUCCESS",
    payload: payload,
  };
};

const connectFailed = (payload) => {
  return {
    type: "CONNECTION_FAILED",
    payload: payload,
  };
};

const updateAccountRequest = (payload) => {
  return {
    type: "UPDATE_ACCOUNT",
    payload: payload,
  };
};

export const connect = () => {
  return async (dispatch) => {
    dispatch(connectRequest());
    if (window.ethereum) 
    {
      let web3 = new Web3(window.ethereum);
      try 
      {
        //await window.ethereum.enable();
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
        /*const accounts = await window.ethereum.request({
          method: "eth_accounts",
        });*/
        const networkID = await window.ethereum.request({
          method: "net_version",
        });
        console.log(networkID);
        console.log("account is "+accounts[0]);
        if (networkID == 137) 
        {
          const playerToken = new web3.eth.Contract(PlayerToken.abi, "0x93C644aE182762db8Ac80847f6Cb9f83F61eB24B");
          dispatch(connectSuccess({account: accounts[0],playerToken: playerToken, web3: web3,}));
          // listeners
          window.ethereum.on("accountsChanged", (accounts) => {
            dispatch(updateAccount(accounts[0]));
          });
          window.ethereum.on("chainChanged", () => {
            window.location.reload();
          });
        } 
        else 
        {
          dispatch(connectFailed("Change network to Polygon"));
        }
      } 
      catch (error) 
      {
        dispatch(connectFailed("Something went wrong"));
      }
    } 
    else 
    {
      dispatch(connectFailed("Install Metamask"));
    }
  };
};

export const updateAccount = (account) => {
  return async (dispatch) => {
    dispatch(updateAccountRequest({ account: account }));
    dispatch(fetchData(account));
  };
};
