pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract PlayerToken is ERC721, Ownable {
  constructor(string memory _name, string memory _symbol)
    ERC721(_name, _symbol)
  {}

  uint256 LATESTPLAYERID;

  uint256 fee = 0.01 ether;

  struct Player {
    string name;
    uint256 nID;
    uint256 nDNA;
    uint8 nLevel;
    uint8 nRarity;
    uint256 nAmountFed;
    //uint256 pattack;
    //uint256 mattack;
    //uint256 pdefense;
    //uint256 mdefense;
  }

  Player[] public players;

  event NewPlayer(address indexed owner, uint256 nID, uint256 nDNA);

  function _createRandomNumber(uint256 _modulus) internal view returns (uint256)
  {
    uint256 nRandomNumber = uint256(keccak256(abi.encodePacked(block.timestamp, LATESTPLAYERID, msg.sender, players.length))); //encodes the block timestamp contatenated with the sender address as uint256
    return nRandomNumber % _modulus; //to return a uint256 of _modulus digits
  }

  function updateFee(uint256 _fee) external onlyOwner
  {
    fee = _fee;
  }

  function withdraw() external payable onlyOwner 
  {
    address payable _owner = payable(owner());
    _owner.transfer(address(this).balance);
  }

  function _createPlayer(string memory _name) internal 
  {
    uint8 nRandomRarity = uint8(_createRandomNumber(100));
    uint256 nRandomDNA = _createRandomNumber(10**16); //to get a 16 digits number
    Player memory newPlayer = Player(_name, LATESTPLAYERID, nRandomDNA, 1, nRandomRarity, 0);
    players.push(newPlayer);
    _safeMint(msg.sender, LATESTPLAYERID);
    emit NewPlayer(msg.sender, LATESTPLAYERID, nRandomDNA);
    LATESTPLAYERID++;
  }

  function createRandomPlayer(string memory _name) public payable 
  {
    require(msg.value >= fee);
    _createPlayer(_name);
  }

  function getPlayers() public view returns (Player[] memory) 
  {
    return players;
  }

  function getOwnedPlayers(address _owner) public view returns (Player[] memory) 
  {
    Player[] memory result = new Player[](balanceOf(_owner));
    uint256 counter = 0;
    for (uint256 i = 0; i < players.length; i++) {
      if (ownerOf(i) == _owner) {
        result[counter] = players[i];
        counter++;
      }
    }
    return result;
  }

  // Game Actions
  function levelUp(uint256 _playerID) public 
  {
    require(ownerOf(_playerID) == msg.sender);
    Player storage player = players[_playerID];
    player.nLevel++;
  }

  function changeName(uint256 _playerID, string memory _name) public 
  {
    require(ownerOf(_playerID) == msg.sender);
    Player storage player = players[_playerID];
    player.name = _name;
  }
  
  function resetDNA(uint256 _playerID) public
  {
    require(ownerOf(_playerID) == msg.sender);
    Player storage player = players[_playerID];
    player.nDNA = _createRandomNumber(10**16);   
  }

  function resetRarity(uint256 _playerID) public
  {
    require(ownerOf(_playerID) == msg.sender);
    Player storage player = players[_playerID];
    player.nRarity = uint8(_createRandomNumber(100));
  }

  function feedPlayer(uint256 _playerID) public
  {
    require(ownerOf(_playerID) == msg.sender);
    Player storage player = players[_playerID];
    player.nAmountFed += 30;
    if(player.nAmountFed >= 100)
    {
      player.nLevel++;
      player.nAmountFed -= 100;
    }
  }
}
